#include <iostream>
#include <string>

using namespace std;

string calc_align(string sseq, string lseq) {
	// keep score of alignment
	int** score = new int*[lseq.size() + 1];
	for(int i = 0; i <= lseq.size(); ++i)
	    score[i] = new int[sseq.size() + 1];
	// keep starting point of alignment in longer sequence
	int** start = new int*[lseq.size() + 1];
	for(int i = 0; i <= lseq.size(); ++i)
	    start[i] = new int[sseq.size() + 1];
	// keep ending point of alignment in longer sequence
	int** end = new int*[lseq.size() + 1];
	for(int i = 0; i <= lseq.size(); ++i)
	    end[i] = new int[sseq.size() + 1];

	// i1=long, i2=short index
	for (int i1 = 0; i1 <= lseq.size(); ++i1) {
		for (int i2 = 0; i2 <= sseq.size(); ++i2) {
			// if short sequence is not started no scores are given (do not deduct if starting gaps)
			if (i2 == 0) {
				score[i1][i2] = 0;
				start[i1][i2] = i1;
				end[i1][i2] = i1;
			}
			// if short sequence is being lost, mismatch score
			if (i2 > 0 && i1 == 0) {
				score[i1][i2] = score[i1][i2 - 1] - 1;
				start[i1][i2] = 0;
				end[i1][i2] = i1;
			}

			if (i2 > 0 && i1 > 0) {
				int max_score = -3000000;

				// if matched:
				int match_score = score[i1 - 1][i2 - 1];
				if (lseq[i1-1] == sseq[i2-1]) match_score += 1;
				else match_score -= 1;
				if (max_score < match_score) max_score = match_score;

				// if short skipped
				int skip_short_score = score[i1][i2 - 1] - 1;
				if (max_score < skip_short_score) max_score = skip_short_score;

				// if long skipped (do not deduct if ending gaps)
				int skip_long_score = score[i1 - 1][i2] - 1;
				if (i2 == sseq.size()) skip_long_score += 1;
				if (max_score < skip_long_score) max_score = skip_long_score;

				// now fill the tables
				score[i1][i2] = max_score;
				if (max_score == match_score) {
					start[i1][i2] = start[i1 - 1][i2 - 1];
					end[i1][i2] = i1;
				} else if (max_score == skip_short_score) {
					start[i1][i2] = start[i1][i2 - 1];
					end[i1][i2] = end[i1][i2 - 1];
				} else if (max_score == skip_long_score) {
					start[i1][i2] = start[i1 - 1][i2];
					if (i2 == sseq.size()) end[i1][i2] = end[i1 - 1][i2];
					else end[i1][i2] = i1;
				}
			}
			//cout << "i1: " <<  i1  << ", i2: " << i2 << "\n";
			//cout << score[i1][i2] << "\t";
		}
		//cout << "\n";
	}
	//cout << "score: " <<  score[lseq.size()][sseq.size()] << "\n";
	// report the aligned substring of the long sequence
	int aln_start = start[lseq.size()][sseq.size()];
	int aln_end = end[lseq.size()][sseq.size()];
	int aln_len = aln_end - aln_start;
	return lseq.substr(aln_start, aln_len);
}

int main() {
	string sseq, lseq;
	cin >> lseq;
	cin >> sseq;
	cout << calc_align(sseq, lseq) << "\n";
	return 0;
}
